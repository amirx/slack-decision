from flask.json import jsonify
from app.models import LunchDecision, LunchOption
from app.lunch_picker import bp
from app import db
from datetime import datetime, timedelta
from flask import request


def build_msg(lunch_decision):

    msg = {
    "text": "Lets choose the best place to eat:",
    "attachments" : [
            {
                "text": "Choose a game to play",
                "fallback": "game fallback",
                'callback_id': 'button_test',
                "actions": []
            }
        ]
    }
    options = LunchOption.query.filter_by(lunch_decision_id=lunch_decision.id)
    for option in options:
        button = {
            "name": "option",
            "text": option.name,
            "type": "button",
            "value": option.name
        }
        msg["attachments"][0]['actions'].append(button)

    return msg


@bp.route("/lunch", methods=['GET','POST'])
def lunch():
    cur_date = datetime.utcnow()
    print ("cur_date is ", cur_date)
    lunch_decision = db.session.query(LunchDecision).filter(
            LunchDecision.decision_date >= cur_date - timedelta(hours=18)
        ).first();
    print(lunch_decision)
    if lunch_decision is None:
        lunch_decision = LunchDecision()
        db.session.add(lunch_decision)
        db.session.commit()
        print("new lunch decision :", lunch_decision.id)
    else:
        print("using :", lunch_decision.id)

    # print(request.form.get('token'))#whNy2xqXda4uGhJjvI5hyCx1
    # channel = request.form.get('channel_name')
    # username = request.form.get('user_name')
    text = request.form.get('text')
    print(text)
    if text is not None:
        args = text.split(" ",1)
        command = args[0]
        if command == "add":
            name = args[1]
            lunch_option = LunchOption(lunch_decision, name)
            db.session.add(lunch_option)
            db.session.commit()

    return jsonify(build_msg(lunch_decision))

