from flask import Blueprint

bp = Blueprint('lunch_picker', __name__)

from app.lunch_picker import routes
