import unittest
from freezegun import freeze_time
from config import Config
from app import create_app, db
from app.models import LunchDecision
from app.models import LunchOption

class TestConfig(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'sqlite://'
    ELASTICSEARCH_URL = None


class Test_lunch_picker(unittest.TestCase):

    def setUp(self):
        self.app = create_app(TestConfig)
        self.client = self.app.test_client()
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_lunch_endpoint_creates_decision(self):
        rv = self.client.get("/lunch")
        count = db.session.query(LunchDecision).count()
        self.assertEqual(1, count)
        assert b'Lets choose the best place to eat:' in rv.data

    def test_two_lunch_calls_in_the_same_day_creates_one_lunch(self):
        @freeze_time("2012-01-14 12:00:01")
        def launch_today():
            self.client.get("/lunch")
        launch_today()

        @freeze_time("2012-01-14 12:00:02")
        def launch_yesterday():
            self.client.get("/lunch")
        launch_yesterday()

        count = db.session.query(LunchDecision).count()

        self.assertEqual(1, count)

    def test_two_lunch_calls_in_two_days_create_two_lunch(self):
        @freeze_time("2012-01-14")
        def launch_today():
            self.client.get("/lunch")
        launch_today()

        @freeze_time("2012-01-15")
        def launch_yesterday():
            self.client.get("/lunch")
        launch_yesterday()

        count = db.session.query(LunchDecision).count()
        all_decisions = LunchDecision.query.all()
        self.assertEqual(2, count)

    @freeze_time("2012-01-14")
    def test_lunch_add_food_option(self):
        self.client.post(
            "/lunch",
            data=dict(
                channel_name='general',
                user_name='testuser',
                text='add good foods'
            )
        )

        self.assertEqual(1, db.session.query(LunchOption).count())
        lunchOption = LunchOption.query.first();
        self.assertEqual("good foods", lunchOption.name)

    @freeze_time("2012-01-14")
    def test_slack_lunch_endpoint_shows_options(self):
        self.client.post(
            "/lunch",
            data=dict(
                channel_name='general',
                user_name='testuser',
                text='add morefoods'
            )
        )
        return_msg = self.client.post(
            "/lunch",
            data=dict(
                channel_name='general',
                user_name='testuser',
                text='add lessfoods'
            )
        )

        self.assertIn("morefoods", str(return_msg.data))
        self.assertIn("lessfoods", str(return_msg.data))
