"""add_lunch_decision

Revision ID: 7ef397e6ab95
Revises: 834b1a697901
Create Date: 2018-08-21 22:11:43.788369

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '7ef397e6ab95'
down_revision = '834b1a697901'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('lunch_decision',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('decision_date', sa.DateTime(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('lunch_decision')
    # ### end Alembic commands ###
